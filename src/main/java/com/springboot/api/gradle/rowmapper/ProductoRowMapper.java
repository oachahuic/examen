package com.springboot.api.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.gradle.model.Producto;

public class ProductoRowMapper implements RowMapper<Producto>{

	@Override
	public Producto mapRow(ResultSet rs, int rowNum) throws SQLException {
		Producto producto = new Producto();
		
		producto.setId(rs.getInt("id"));
		producto.setDescripcion(rs.getString("descripcion"));
		producto.setCategoria(rs.getString("categoria"));
		producto.setPrecio_unitario(rs.getDouble("precio_unitario"));
		producto.setStock_actual(rs.getInt("stock_actual"));
		producto.setStock_minimo(rs.getInt("stock_minimo"));
		producto.setEstado(rs.getString("estado"));
		
		return producto;
	}

}
